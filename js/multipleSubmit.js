(function ($, Drupal, once) {
  Drupal.behaviors.multipleSubmit = {
    attach: function attach(context, settings) {

      var $context = $(context);

      const $elements =  $(once('body', '.view-buscador-exposed-form-region', context));

      $elements.each(function () {
        var fields = [];

        pushValues();

        $(this).find('[drupal-data-efr-include-submit]').find(':input.form-checkbox').on('change', function (event) {
          let brother_checkbox = $('input[name="'+$(this).attr('name')+'"]:not(#'+$(this).attr('id')+')');
          brother_checkbox.prop("checked", $(this).prop("checked"));
        });

        $(this).find('[drupal-data-efr-include-submit]').find(':input').on('change', function (event) {
          pushValues();
        });

        $(this).find('[drupal-data-efr-include-submit]').find('select').on('change', function (event) {
          pushValues();
        });

        $(this).find('[drupal-data-efr-include-submit]').find('.form-type-textfield input').on('focusout', function (event) {
          pushValues();
        });
        $(this).find('[drupal-data-efr-submit]').on('click', function (event) {
          event.preventDefault();
          let form = $(this).closest('form');
          addFieldsToForm(form, fields);
          form.trigger("submit");
        });

        function pushValues(){

          $context.find('[drupal-data-efr-include-submit]').find(':input, select, .form-type-textfield input').each(function () {
            let type = $(this).attr('type');
            let name = $(this).attr('name');
            if((type !== 'hidden' && type !== 'submit') &&
              ((type !== 'checkbox' && type !== 'radio') || $(this).is(':checked'))
            ) {
              let val = $(this).val();
              fields[name] = $('<input>').attr('type', 'hidden').attr('name', name).val(val);
            }
          });
        }

        function addFieldsToForm(form, fields){
          for (let value in fields) {
            if(form.find(':input[name="'+value+'"]').length < 1){
              form.append(fields[value]);
            }
          };
        }
      });
    }
  };
})(jQuery, Drupal, once);
