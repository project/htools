(function ($, Drupal, drupalSettings) {

  var interval = drupalSettings.bigPipeInterval || 50;

  var timeoutObserverID = void 0;

  function bigPipeObserverProcessDocument(context) {
    if (!context.querySelector('script[data-big-pipe-event="start"]')) {
      return false;
    }

    if (context.querySelector('script[data-big-pipe-event="stop"]')) {
      if (timeoutObserverID) {
        clearTimeout(timeoutObserverID);
        $('body').trigger('big_pipe:finish');
      }
      return true;
    }

    return false;
  }

  function bigPipeObserverProcess() {
    timeoutObserverID = setTimeout(function () {
      if (!bigPipeObserverProcessDocument(document)) {
        bigPipeObserverProcess();
      }
    }, interval);
  }

  bigPipeObserverProcess();

  $(window).on('load', function () {
    if (timeoutObserverID) {
      clearTimeout(timeoutObserverID);
    }
    bigPipeObserverProcessDocument(document);
  });

})(jQuery, Drupal, drupalSettings);