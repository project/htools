/**
 * @file
 */
(function ($, Drupal, drupalSettings) {

  Drupal.sortBehaviors = function (sort_list) {
    list = [];
    for (let k in Drupal.behaviors) {
      let weight = 0;
      if (sort_list.hasOwnProperty(k)) {
        weight = sort_list[k];
      }
      list.push({"weight": weight, "name": k, "behavior": Drupal.behaviors[k]});
    }
    if (list.length > 0) {
      list.sort(function (a, b) {
        if (a.weight > b.weight) {
          return 1;
        }
        if (a.weight < b.weight) {
          return -1;
        }
        return 0;
      });
      Drupal.behaviors = {};
      for (let k in list) {
        let key = list[k]['name'];
        Drupal.behaviors[key] = list[k]['behavior'];
      }
    }
  };

  Drupal.sortBehaviors(drupalSettings.behaviorHandler.sort);
  if (drupalSettings.hasOwnProperty('bigPipePlaceholderIds')) {
    // If bigpipe is enabled, first attach behaviors of main chunk before other chunks.
    Drupal.attachBehaviors(document, drupalSettings);
  }

}(jQuery, Drupal, drupalSettings));
