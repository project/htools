(function ($, Drupal) {
  Drupal.AjaxCommands.prototype.downloadFiles = function (ajax, response, status) {
    var link = document.createElement('a');
    link.setAttribute('target', '_blank');
    link.setAttribute('href', response.download_url);
    link.setAttribute('download', response.file_name);
    link.click();
  }
})(jQuery, Drupal);
