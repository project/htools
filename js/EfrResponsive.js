(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.responsiveFilters = {
    attach: function attach(context) {
      var $context = $(context);

      $context.find('.mobile__filters').once('responsivefilters').each(function () {
        var showFilters = document.querySelector('.mobile__filters');
        var filters = document.querySelector('.view-filters-2');
        var cross = document.querySelector('.view-filters-2 .mobile__header .close');

        // if(!showFilters || !filters || !cross) return;
        showFilters.onclick = function () {
          filters.classList.add('open');
          document.body.classList.add('filters');
          document.querySelector('html').classList.add('filters');
        };

        cross.onclick = function () {
          filters.classList.remove('open');
          document.body.classList.remove('filters');
          document.querySelector('html').classList.remove('filters');
        };
      });
    }
  };
})(jQuery, Drupal);