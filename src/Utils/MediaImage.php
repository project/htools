<?php

namespace Drupal\htools\Utils;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\media\MediaInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\file\FileInterface;
use Drupal\media\Entity\Media;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Util to deal with media images.
 */
class MediaImage {

  /**
   * Configuration manager service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configuration;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The entity-type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $configuration, LanguageManagerInterface $language_manager, AccountProxyInterface $account = NULL, EntityTypeManagerInterface $entity_type_manager) {
    $this->configuration = $configuration;
    $this->languageManager = $language_manager;
    $this->account = $account;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Create media instance.
   *
   * @param string $url
   *   The remote URL.
   * @param \Drupal\file\FileInterface|null $file
   *   The file instance or NULL if the file will be lazy loaded.
   * @param string $alt
   *   The file alt and title string, if empty the filename will be used.
   * @param array $properties
   *   An array with properties for creating the media.
   *
   * @return \Drupal\media\MediaInterface
   *   The media instance.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createMedia($url, $file = NULL, $alt = NULL, $properties = NULL): MediaInterface {
    $langcode = $this->languageManager->getDefaultLanguage()->getId();

    $values = [
      'bundle' => empty($properties['bundle']) ? 'image' : $properties['bundle'],
      'uid' => $this->account->id(),
      'langcode' => $langcode,
      'status' => 1,
    ];

    if ($file instanceof FileInterface) {
      $alt = $alt ?: $file->getFilename();
      $field = empty($properties['media_field']) ? 'image' : $properties['media_field'];
      $values[$field] = [
        'target_id' => $file->id(),
        'alt' => $alt,
        'title' => $alt,
      ];
      $values['name'] = $alt . ' - ' . $file->id();
    }

    $media = Media::create($values);
    if (!empty($url)) {
      $media->set('remote_source', $url);
    }
    $media->save();

    return $media;
  }

  /**
   * Lookup the media entity using the source URL.
   *
   * @param string $url
   *   The source URL.
   *
   * @return \Drupal\media\MediaInterface|null
   *   The media entity instance, or NULL if cannot be found.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function lookupEntity($url): ?MediaInterface {
    $storage = $this->entityTypeManager->getStorage('media');
    $query = $storage->getQuery()
      ->condition('remote_source', $url);
    $result = $query->execute();
    $result = reset($result);

    if (empty($result)) {
      return NULL;
    }

    /** @var \Drupal\media\MediaInterface $media */
    $media = $storage->load($result);

    return $media;
  }

}
