<?php

namespace Drupal\htools\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Defines an AJAX command that download a file.
 *
 * @ingroup ajax
 */
class DownloadFileCommand implements CommandInterface {

  /**
   * A CSS selector string of the dialog to close.
   *
   * @var string
   */
  protected $download_url;

  /**
   * A CSS selector string of the dialog to close.
   *
   * @var string
   */
  protected $file_name;


  /**
   * Constructs a DownloadFileCommand object.
   *
   * @param string $download_url
   *   A CSS selector string of the dialog to close.
   *
   * @param string $file_name
   *   A CSS selector string of the dialog to close.
   */
  public function __construct($download_url, $file_name) {
    $this->download_url = $download_url;
    $this->file_name = $file_name;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'downloadFiles',
      'download_url' => $this->download_url,
      'file_name' => $this->file_name,
    ];
  }
}
