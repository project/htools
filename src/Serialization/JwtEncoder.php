<?php
namespace Drupal\htools\Serialization;

use Drupal\Component\Serialization\SerializationInterface;
use \Firebase\JWT\JWT;

class JwtEncoder implements SerializationInterface {

  /**
   * Get the key to encode with JWT.
   *
   * @return string
   *   The jwt key to encode.
   */
  private static function getJwtKey() {
    return \Drupal::configFactory()->get('system.site')->get('uuid');
  }
  /**
   * Decode jwt.
   *
   * @param string $jwt
   *   The string to decode.
   *
   * @return string
   *   The object from JWT.
   */
  public static function decode($jwt) {
    $key = static::getJwtKey();

    $algorithm = ['HS256'];
    if ($key) {
      return JWT::decode($jwt, $key, $algorithm);
    }
  }
  /**
   * Encode jwt.
   *
   * @param array|object $jwt
   *   The string to encode.
   *
   * @return string
   *   The JWT object.
   */
  public static function encode($jwt) {
    $key = static::getJwtKey();

    return JWT::encode($jwt, $key);
  }

  /**
   * {@inheritdoc}
   */
  public static function getFileExtension() {
    return 'jwt';
  }
}
