<?php

namespace Drupal\htools\Plugin\ViewsReferenceSetting;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\views\ViewExecutable;
use Drupal\viewsreference\Plugin\ViewsReferenceSettingInterface;

/**
 * The views reference setting pager plugin.
 *
 * @ViewsReferenceSetting(
 *   id = "exposed_filters_path",
 *   label = @Translation("Exposed Filters Path"),
 *   default_value = "",
 * )
 */
class ViewsReferenceExposedFiltersPath extends PluginBase implements ViewsReferenceSettingInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function alterFormField(array &$form_field) {
    $form_field['#type'] = 'linkit';
    $form_field['#description'] = $this->t('Start typing to find content.');
    $form_field['#autocomplete_route_name'] = 'linkit.autocomplete';
    $form_field['#autocomplete_route_parameters'] = [
      'linkit_profile_id' => 'default',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function alterView(ViewExecutable $view, $value) {
    if (!empty($value)) {
      $view->display_handler->setOption('path', $value);
    }
  }

}
