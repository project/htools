<?php

namespace Drupal\htools\Plugin\ViewsReferenceSetting;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\views\ViewExecutable;
use Drupal\viewsreference\Plugin\ViewsReferenceSettingInterface;

/**
 * The views reference setting view more plugin.
 *
 * @ViewsReferenceSetting(
 *   id = "view_create_view_more",
 *   label = @Translation("Create more link"),
 *   default_value = "",
 * )
 */
class ViewsReferenceCreateViewMore extends PluginBase implements ViewsReferenceSettingInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function alterFormField(array &$form_field) {
    $form_field['#type'] = 'details';
    $form_field['#open'] = TRUE;
    $form_field['#tree'] = TRUE;
    $form_field['use_more'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create more link'),
      '#description' => $this->t("This will add a more link to the bottom
      of this view, which will link to the page view. If you have more than one
      page view, the link will point to the display specified in 'Link display'
      section under pager. You can override the URL at the link display setting.
      "),
      '#default_value' => $form_field['#default_value']['use_more'] ?? 0
    ];

    $form_field['use_more_always'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Always display the more link'),
      '#description' => $this->t('Check this to display the more link even if there are no more items to display.'),
      '#states' => [
        'visible' => [
          ':input[name$="[options][view_create_view_more][use_more]"]' => ['checked' => TRUE],
        ],
      ],
      '#default_value' => $form_field['#default_value']['use_more_always'] ?? 0
    ];

    $form_field['use_more_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('More link text'),
      '#description' => $this->t("The text to display for the more link."),
      '#states' => [
        'visible' => [
          ':input[name$="[options][view_create_view_more][use_more]"]' => ['checked' => TRUE],
        ],
      ],
      '#default_value' => $form_field['#default_value']['use_more_text'] ?? ''
    ];

    $form_field['link_url'] = [
      '#type' => 'linkit',
      '#title' => $this->t('Custom URL'),
      '#description' => $this->t("A Drupal path or external URL
      the more link will point to. Note that this will override the link display
      setting above."),
      '#autocomplete_route_name' => 'linkit.autocomplete',
      '#autocomplete_route_parameters' => ['linkit_profile_id' => 'default'],
      '#states' => [
        'visible' => [
          ':input[name$="[options][view_create_view_more][use_more]"]' => ['checked' => TRUE],
        ],
      ],
      '#default_value' => $form_field['#default_value']['link_url'] ?? ''
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function alterView(ViewExecutable $view, $value) {
    if (!empty($value)) {
      if (isset($value['use_more'])) {
        $view->display_handler->setOption('use_more', $value['use_more']);
      }
      if (isset($value['use_more_always'])) {
        $view->display_handler->setOption('use_more_always', $value['use_more_always']);
      }
      if (!empty($value['use_more_text'])) {
        $view->display_handler->setOption('use_more_text', $value['use_more_text']);
      }
      if (!empty($value['link_url'])) {
        $view->display_handler->setOption('link_display', 'custom_url');
        $view->display_handler->setOption('link_url', $value['link_url']);
      }
    }
  }

}
