<?php

namespace Drupal\htools\Plugin\ViewsReferenceSetting;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\htools\Plugin\views\display\LayoutBuilder;
use Drupal\views\ViewExecutable;
use Drupal\viewsreference\Plugin\ViewsReferenceSettingInterface;

/**
 * The views reference setting pager plugin.
 *
 * @ViewsReferenceSetting(
 *   id = "region_visualization",
 *   label = @Translation("Region Visualization"),
 *   default_value = {
 *     "rows",
 *     "pager"
 *   }
 * )
 */
class ViewsReferenceRegionsVisualization extends PluginBase implements ViewsReferenceSettingInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function alterFormField(array &$form_field) {
    $form_field['#type'] = 'checkboxes';
    $form_field['#description'] = $this->t('Check the regions that you want to show.');
    $form_field['#options'] = LayoutBuilder::getRegions();
  }

  /**
   * {@inheritdoc}
   */
  public function alterView(ViewExecutable $view, $value) {
    if (!empty($value)) {
      $view->display_handler->setOption('regions', $value);
    }
  }

}
