<?php

namespace Drupal\htools\Plugin\Derivative;

use Drupal\Core\Plugin\Context\EntityContextDefinition;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\layout_builder\Plugin\Derivative\FieldBlockDeriver;

/**
 * Provides entity type block definitions for every entity type.
 *
 * @internal
 *   Plugin derivers are internal.
 */
class EntityTypeBlockDeriver extends FieldBlockDeriver implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The entity type repository.
   *
   * @var \Drupal\Core\Entity\EntityTypeRepositoryInterface
   */
  protected $entityTypeRepository;

  /**
   * The field type manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypeManager;

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $entity_type_labels = $this->entityTypeRepository->getEntityTypeLabels();
    foreach ($this->entityFieldManager->getFieldMap() as $entity_type_id => $entity_field_map) {

      $derivative = $base_plugin_definition;
      $derivative['admin_label'] = $entity_type_labels[$entity_type_id];
      $context_definition = EntityContextDefinition::fromEntityTypeId($entity_type_id)
        ->setLabel($entity_type_labels[$entity_type_id]);

      $derivative['context_definitions'] = [
        'entity' => $context_definition,
      ];

      $derivative_id = $entity_type_id;
      $this->derivatives[$derivative_id] = $derivative;

    }
    return $this->derivatives;
  }

}
