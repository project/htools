<?php

namespace Drupal\htools\Plugin\DownloadOption;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Unicode;
use Drupal\Console\Bootstrap\Drupal;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\file\FileInterface;
use Drupal\file_downloader\Annotation\DownloadOption;
use Drupal\file_downloader\DownloadOptionPluginBase;
use Drupal\file_downloader\Plugin\DownloadOption\OriginalFile;
use Drupal\image\ImageStyleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Drupal\file_downloader\Entity\DownloadOptionConfigInterface;

/**
 * Defines a download option plugin.
 *
 * @DownloadOption(
 *   id = "form_file_download",
 *   label = @Translation("Form File Download"),
 *   description = @Translation("Download a file once the user has filled a form."),
 * )
 */
class FormFileDownload extends DownloadOptionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * @inheritdoc
   */
  public function downloadOptionForm($form, FormStateInterface $form_state) {

    $roles = array_map(['\Drupal\Component\Utility\Html', 'escape'], user_role_names(TRUE));
    $form['roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles'),
      '#options' => $roles,
      '#access' => TRUE,
      '#default_value' => $this->getConfigurationValue('roles'),
      '#description' => $this->t('This users are allowed to download the file without filling a form'),
    ];

    $form_bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo('contact_message');
    $options = [];
    foreach ($form_bundles as $key => $value) {
      $options[$key] = $value['label'];
    }

    $form['form_bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Form Bundle'),
      '#options' => $options,
      '#required' => TRUE,
      '#empty_option' => $this->t('Please select a form bundle.'),
      '#default_value' => $this->getConfigurationValue('form_bundle'),
    ];

    return $form;
  }

  /**
   * @inheritdoc
   */
  public function defaultConfiguration() {
    return array(
      'id' => $this->getPluginId(),
      'roles' => [],
      'form_bundle' => '',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function downloadOptionSubmit($form, FormStateInterface $form_state) {
    $this->configuration['roles'] = $form_state->getValue('roles');
    $this->configuration['form_bundle'] = $form_state->getValue('form_bundle');
  }

  public function buildLink(DownloadOptionConfigInterface $downloadOptionConfig, $file_id){
    $settings = $downloadOptionConfig->get('settings');
    $opt = [
      'query' => [
        'dialogOptions' => ['classes' =>  ['ui-dialog' => 'htools-form-file-download']]
      ]
    ];
    /** @var \Drupal\link\LinkItemInterface $content */
    $url = Url::fromRoute('htools.contact_form.canonical', [
      'contact_form' => $settings['form_bundle'],
      'file' => $file_id,
    ], $opt);


    $downloadLink = [
      '#type' => 'link',
      '#title' => $downloadOptionConfig->label(),
      '#url' => $url,
      '#attributes' => [
        'class' => 'use-ajax',
        'data-dialog-type' => 'dialog',
        'data-dialog-options' =>  Json::encode([
          'position' => 'fixed',
          'height' => 'auto',
          'width' => '768px',
        ]),
      ],
      '#attached' => [
        'library' => ['htools/download_file']
      ],
    ];

    return $downloadLink;
  }

  public function access(AccountInterface $account, FileInterface $file) {

    $settings = $this->getConfiguration();

    $current_roles = $account->getRoles();
    $allowed_roles = array_values($settings['roles']);
    $user_allowed = false;

    foreach ($current_roles as $key => $value) {
      if (in_array($value, $allowed_roles, TRUE)) {
        $user_allowed = true;
      }
    }

    if ($user_allowed == true) {
      return AccessResult::allowed();
    }
    else {
      return AccessResult::forbidden('User has no permission to view the original file.');
    }

  }

}
