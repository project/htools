<?php

namespace Drupal\htools\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'ProcessedTextBlock' block.
 *
 * @Block(
 *  id = "processed_text",
 *  admin_label = @Translation("Processed Text"),
 *  category = @Translation("Hiberus tools"),
 * )
 */
class ProcessedTextBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['content' => ['value' => '', 'format' => filter_default_format()]];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $settings = $this->getConfiguration();
    $form['content'] = [
      '#title' => $this->t('Content'),
      '#type' => 'text_format',
      '#default_value' => !empty($settings['content']['value']) ? $settings['content']['value'] : NULL,
      '#format' => !empty($settings['content']['format']) ? $settings['content']['format'] : filter_default_format(),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * Most block plugins should not override this method. To add submission
   * handling for a specific block type, override BlockBase::blockSubmit().
   *
   * @see \Drupal\Core\Block\BlockBase::blockSubmit()
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $this->configuration['content'] = $form_state->getValue('content');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $settings = $this->getConfiguration();

    $content = $settings['content']['value'];
    $build['html_block'] = [
      '#type' => 'processed_text',
      '#text' => $content,
      '#format' => $settings['content']['format'],
      '#filter_types_to_skip' => [],
    ];

    return $build;
  }

}
