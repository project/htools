<?php

namespace Drupal\htools\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to process Twig.
 *
 * @Filter(
 *   id = "filter_twig",
 *   title = @Translation("Process twig code"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   weight = 3
 * )
 */
class FilterTwig extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $environment = \Drupal::service('twig');
    $text = $environment->renderInline($text);
    return new FilterProcessResult($text);
  }

}
