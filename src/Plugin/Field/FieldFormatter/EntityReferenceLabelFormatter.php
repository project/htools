<?php

namespace Drupal\htools\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter as CoreEntityReferenceLabelFormatter;
use Drupal\Core\Url;

class EntityReferenceLabelFormatter extends CoreEntityReferenceLabelFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    $output_as_link = $this->getSetting('link');
    if (!$output_as_link) {
      return $elements;
    }
    $current_url = Url::fromRoute('<current>');
    $current_url_options = $current_url->getOptions();

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      if (!empty($elements[$delta]['#type']) && $elements[$delta]['#type'] === 'link' && $elements[$delta]['#url'] instanceof Url) {
        $url = $elements[$delta]['#url'];
        if ($url->toString() === $current_url->toString()) {
          $elements[$delta]['#attributes']['class'][] = 'active';
        }
      }
    }

    return $elements;
  }

}
