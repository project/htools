<?php

namespace Drupal\htools\Plugin\Field\FieldFormatter;

use Drupal\Core\Access\AccessResultForbidden;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\file\FileInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file_downloader\Plugin\Field\FieldFormatter\FileDownloaderFieldFormatter as OriginalFileDownloaderFieldFormatter;

class FileDownloaderFieldFormatter extends OriginalFileDownloaderFieldFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $settings = $this->getSettings();

    $entityTypeManager = \Drupal::service('entity_type.manager');
    /** @var \Drupal\Core\Entity\EntityStorageInterface $downloadOptionConfigStorage */
    $downloadOptionConfigStorage = $entityTypeManager->getStorage('download_option_config');
    $downloadOptionConfigEntities = $downloadOptionConfigStorage->loadMultiple($settings['download_options']);

    /** @var \Drupal\file\FileInterface $file */
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $renderArray = $this->getDownloadLinksArray($downloadOptionConfigEntities, $file);

      if (empty($renderArray)) {
        continue;
      }
      $elements[$delta] = $renderArray;
    }

    return $elements;
  }

  /**
   * Get a item list containing download links.
   *
   * @param $downloadOptionConfigEntities
   * @param $file
   *
   * @return mixed
   */
  private function getDownloadLinksArray($downloadOptionConfigEntities, FileInterface $file) {
    $download_links = $this->getDownloadPluginLinks($downloadOptionConfigEntities, $file);

    if (empty($download_links)) {
      return [];
    }

    return [
      '#theme' => 'file_download_list',
      '#content' => [
        '#theme' => 'item_list',
        '#theme_wrappers' => [],
        '#attributes' => [
          'class' => [
            'download-options-list',
          ],
        ],
        '#items' => $download_links,
        '#cache' => [
          'tags' => $file->getCacheTags(),
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  private function getDownloadPluginLinks($downloadOptionConfigEntities, FileInterface $file) {
    $download_links = [];

    /** @var \Drupal\file_downloader\Entity\DownloadOptionConfigInterface $downloadOptionConfig */
    foreach ($downloadOptionConfigEntities as $downloadOptionConfig) {
      $disabled = FALSE;
      $downloadOptionPlugin = $downloadOptionConfig->getPlugin();

      $file_id = $file->id();

      $account = \Drupal::currentUser();
      $settings = $downloadOptionConfig->get('settings');
      $downloadOptionPlugin->setConfiguration($settings);
      if (method_exists($downloadOptionPlugin, 'buildLink') && $downloadOptionPlugin->access($account, $file) instanceof AccessResultForbidden) {
        $downloadLink = $downloadOptionPlugin->buildLink($downloadOptionConfig, $file_id);
      }
      else {
        /** @var \Drupal\link\LinkItemInterface $content */
        $url = Url::fromRoute('download_option_config.download_path', [
          'download_option_config' => $downloadOptionConfig->id(),
          'file' => $file->id(),
        ]);


        $downloadLink = [
          '#type' => 'link',
          '#title' => $downloadOptionConfig->label(),
          '#url' => $url,
        ];
      }

      $theme = 'file_download_link';

      $download_links[] = [
        '#theme' => $theme,
        '#content' => $downloadLink,
        '#file' => $file,
        '#downloadOptionConfig' => $downloadOptionConfig,
        '#disabled' => $disabled,
        '#cache' => [
          'contexts' => $file->getCacheContexts() + $downloadOptionConfig->getCacheContexts(),
          'tags' => $file->getCacheTags() + $downloadOptionConfig->getCacheTags(),
        ],
      ];
    }

    return $download_links;
  }

}
