<?php


namespace Drupal\htools\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slick\Plugin\Field\FieldFormatter\SlickEntityFormatterBase;
use Drupal\slick\SlickDefault;

/**
 * Plugin implementation of the 'Slick Entity Reference' formatter.
 *
 * @FieldFormatter(
 *   id = "slick_asnavfor",
 *   label = @Translation("Slick AsNavFor"),
 *   description = @Translation("Display the vanilla entity reference/entity
 *   reference revision as a Slick carousel."), field_types = {
 *     "entity_reference",
 *     "entity_reference_revisions"
 *   }
 * )
 */
class SlickAsNavForFormatter extends SlickEntityFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = SlickDefault::baseSettings();
    $settings['view_mode'] = '';

    $settings['main'] = SlickDefault::baseSettings();
    $settings['main']['view_mode'] = '';

    $settings['pager'] = SlickDefault::baseSettings();
    $settings['pager']['view_mode'] = '';

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = [
      'main' => [
        '#type' => 'details',
        '#title' => 'Main',
      ],
      'pager' => [
        '#type' => 'details',
        '#title' => 'pager',
      ],
    ];
    $definition = $this->getScopedFormElements();

    $definition['_views'] = isset($form['field_api_classes']);


    $this->admin()->buildSettingsForm($element['main'], $definition);
    $this->admin()->buildSettingsForm($element['pager'], $definition);

    foreach ($definition["settings"]["main"] as $key => $value) {
      $element['main'][$key]['#default_value'] = $value;
    }
    foreach ($definition["settings"]["pager"] as $key => $value) {
      $element['pager'][$key]['#default_value'] = $value;
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $entities = $this->getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($entities)) {
      return [];
    }

    // Collects specific settings to this formatter.
    $widget_settings = $this->getSettings();
    $settings_main = $widget_settings['main'];
    $settings_pager = $widget_settings['pager'];

    // Asks for Blazy to deal with iFrames, and mobile-optimized lazy loading.
    $settings['blazy'] = TRUE;
    $settings['plugin_id'] = $this->getPluginId();
    $settings['vanilla'] = TRUE;
    $settings['view_mode'] = $settings_main['view_mode'];
    $settings['nav'] = TRUE;
    $settings['optionset'] = $settings_main['optionset'];
    $settings['optionset_thumbnail'] = $settings_pager['optionset'];
    $settings['skin_thumbnail'] = $settings_pager['skin'];
    $settings['navpos'] = TRUE;
    $build = ['settings' => $settings];

    $this->formatter->buildSettings($build, $items);
    $build['settings']['nav'] = TRUE;
    // Build the elements.
    $this->buildElements($build, $entities, $langcode);
    $build['thumb']['settings'] = $widget_settings['pager'];
    $build['thumb']['items'] = $build['items'];
    foreach ($build['thumb']['items'] as $key => $item) {
      $entity = $entities[$key];
      $build['thumb']['items'][$key] = $this->manager()
        ->getEntityTypeManager()
        ->getViewBuilder($entity->getEntityTypeId())
        ->view($entity, $widget_settings['pager']['view_mode'], $langcode);
      $this->manager()
        ->getRenderer()
        ->addCacheableDependency($build['thumb']['items'][$key], $entity);
    }
    return $this->manager()->build($build);
  }

}
