<?php

namespace Drupal\htools\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\contact\ContactFormInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\htools\Form\MessageForm;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Controller\ControllerBase as OriginalControllerBase;
/**
 * Controller routines for contact routes.
 */
class ContactController extends OriginalControllerBase {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a ContactController object.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * Presents the site-wide contact form.
   *
   * @param \Drupal\contact\ContactFormInterface $contact_form
   *   The contact form to use.
   *
   * @return array
   *   The form as render array as expected by
   *   \Drupal\Core\Render\RendererInterface::render().
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   Exception is thrown when user tries to access non existing default
   *   contact form.
   */
  public function contactSitePage(ContactFormInterface $contact_form = NULL, File $file) {
    $config = $this->config('contact.settings');

    // Use the default form if no form has been passed.
    if (empty($contact_form)) {
      $contact_form = $this->entityTypeManager()
        ->getStorage('contact_form')
        ->load($config->get('default_form'));
      // If there are no forms, do not display the form.
      if (empty($contact_form)) {
        if ($this->currentUser()->hasPermission('administer contact forms')) {
          $this->messenger()->addError($this->t('The contact form has not been configured. <a href=":add">Add one or more forms</a> .', [
            ':add' => Url::fromRoute('contact.form_add')->toString(),
          ]));
          return [];
        }
        else {
          throw new NotFoundHttpException();
        }
      }
    }

    $message = $this->entityTypeManager()
      ->getStorage('contact_message')
      ->create([
        'contact_form' => $contact_form->id(),
      ]);

    $class = MessageForm::class;
    $form_state_additions = [];
    $form_state_additions['file'] = $file;
    $form = $this->entityFormBuilder()->getForm($message, 'default', $form_state_additions, $class);
    $form['#title'] = $contact_form->label();
    $form['#cache']['contexts'][] = 'user.permissions';
    $this->renderer->addCacheableDependency($form, $config);
    return $form;
  }

  /**
   * Retrieves the entity form builder.
   *
   * @return \Drupal\Core\Entity\EntityFormBuilderInterface
   *   The entity form builder.
   */
  protected function entityFormBuilder() {
    if (!$this->entityFormBuilder) {
      if (!empty(\Drupal::hasService('efs.entity.form_builder'))) {
        $this->entityFormBuilder = \Drupal::service('efs.entity.form_builder');
      } else {
        $this->entityFormBuilder = \Drupal::service('entity.form_builder');
      }

    }
    return $this->entityFormBuilder;
  }

}
