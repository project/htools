<?php

namespace Drupal\htools\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Form\ViewsExposedForm as CoreViewsExposedForm;

/**
 * Provides the views exposed form.
 *
 * @internal
 */
class ViewsExposedForm extends CoreViewsExposedForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    /** @var \Drupal\views\ViewExecutable $view */
    $view = $form_state->get('view');
    $display = &$form_state->get('display');
    $region = $form_state->get('region');
    if (is_int($region)) {
      $form['#id'] = Html::cleanCssIdentifier('views_exposed_form-' . $view->storage->id() . '-' . $display['id'] . '-region-' . $region);
    }
    else {
      $form['#id'] = Html::cleanCssIdentifier('views_exposed_form-' . $view->storage->id() . '-' . $display['id']);
    }

    return $form;
  }

}
