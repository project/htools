<?php

namespace Drupal\htools\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\htools\Ajax\DownloadFileCommand;
use Drupal\contact\MessageForm as OriginalMessageForm;

/**
 * Form controller for contact message forms.
 *
 * @internal
 */
class MessageForm extends OriginalMessageForm {

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);

    $actions['submit']['#ajax'] = [
      'callback' => [$this, 'submitCallback'],
    ];

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['#prefix'] = '<div id="download-file-contact-form"></div>';

    return $form;
  }

  public function submitCallback(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if ($form_state->getErrors()) {
      $status_messages = ['#type' => 'status_messages'];
      $selector = '#download-file-contact-form';
      $response->addCommand(new HtmlCommand($selector, $status_messages));
      return $response;
    }
    /** @var \Drupal\file\Entity\File $file */
    $file = $form_state->get('file');

    $response->addCommand(new CloseDialogCommand('.ui-dialog-content'));
    $url = file_create_url($file->getFileUri());
    $file_name = $file->getFilename();
    $response->addCommand(new DownloadFileCommand($url, $file_name));
    return $response;
  }

}
