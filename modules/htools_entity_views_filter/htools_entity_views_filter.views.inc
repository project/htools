<?php

/**
 * Implements hook_views_data_alter().
 */
function htools_entity_views_filter_views_data_alter(array &$data)
{

  $data['views']['bundle_summary'] = [
    'title' => t('Bundle Summary'),
    'help' => t('Bundle Summary.'),
    'area' => [
      'id' => 'entity_views_filter',
    ],
  ];

  return $data;

}
