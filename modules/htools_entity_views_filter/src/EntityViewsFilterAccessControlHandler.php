<?php

namespace Drupal\htools_entity_views_filter;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Entity views filter entity.
 *
 * @see \Drupal\htools_entity_views_filter\Entity\EntityViewsFilter.
 */
class EntityViewsFilterAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\htools_entity_views_filter\Entity\EntityViewsFilterInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished entity views filter entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published entity views filter entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit entity views filter entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete entity views filter entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add entity views filter entities');
  }


}
