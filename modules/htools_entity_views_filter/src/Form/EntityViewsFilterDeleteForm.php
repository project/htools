<?php

namespace Drupal\htools_entity_views_filter\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Entity views filter entities.
 *
 * @ingroup entity_views_filter
 */
class EntityViewsFilterDeleteForm extends ContentEntityDeleteForm {


}
