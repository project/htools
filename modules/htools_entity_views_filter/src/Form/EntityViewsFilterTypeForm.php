<?php

namespace Drupal\htools_entity_views_filter\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Views;

/**
 * Class EntityViewsFilterTypeForm.
 */
class EntityViewsFilterTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $definition = $this->getEntity();
    $entity_views_filter_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity_views_filter_type->label(),
      '#description' => $this->t("Label for the Entity views filter type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity_views_filter_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\htools_entity_views_filter\Entity\EntityViewsFilterType::load',
      ],
      '#disabled' => !$entity_views_filter_type->isNew(),
    ];

    $form['submit_button'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Submit button text'),
      '#maxlength' => 255,
      '#default_value' => $entity_views_filter_type->get('submit_button') ? $entity_views_filter_type->get('submit_button') : 'Search',
      '#required' => TRUE,
    ];

    $form['autosubmit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable auto-submit'),
      '#maxlength' => 255,
      '#default_value' => $entity_views_filter_type->get('autosubmit') ? $entity_views_filter_type->get('autosubmit') : FALSE,
    ];

    $form['autosubmit_hide'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide submit button'),
      '#maxlength' => 255,
      '#default_value' => $entity_views_filter_type->get('autosubmit_hide') ? $entity_views_filter_type->get('autosubmit_hide') : FALSE,
      '#states' => [
        'visible' => [
          '[name="autosubmit"]' => ['checked' => TRUE],
        ],
      ],
    ];

    /* You will need additional form elements for your custom properties. */

    if ($definition->id() != NULL) {
      $views_list = Views::getViewsAsOptions(TRUE);

      $form['view_name'] = [
        '#type' => 'select',
        '#options' => $views_list,
        '#title' => $this->t('View Name'),
        '#maxlength' => 255,
        '#default_value' => $definition->get('view_name'),
        '#ajax' => [
          'callback' => '::changeDisplay',
          'event' => 'change',
          'wrapper' => 'display-id',
        ],
      ];

      if (($definition->view_name) != NULL) {
        $views_info = Views::getView($definition->get('view_name'));
        $views_displays = $views_info->storage->get('display');
        foreach ($views_displays as $display) {
          $displays_array[$display['id']] = $display['id'];
        }
        $form['view_display_id'] = [
          '#type' => 'select',
          '#options' => $displays_array,
          '#title' => $this->t('View Display ID'),
          '#maxlength' => 255,
          '#default_value' => $definition->get('view_display_id'),
          '#prefix' => '<div id="display-id">',
          '#suffix' => '</div>',
        ];
      }

    }


    if (($definition->get('view_display_id') != NULL) && ($definition->get('view_name') != NULL)) {
      $view_name = $definition->get('view_name');
      $view_display_id = $definition->get('view_display_id');
      $view = Views::getView($view_name);
      $view->build($view_display_id);
      $filters = $view->getHandlers('filter');
      $sorts = $view->getHandlers('sort');
      $exposed_form_plugin = $view->display_handler->getPlugin('exposed_form');
      $view_url = $view->getPath();
      $field_mapping = $definition->get('field_mapping');
      $form['field_mapping'] = [
        '#type' => 'details',
        '#title' => $this->t('Field Mapping'),
        '#open' => TRUE,
        '#tree' => TRUE,
      ];
      foreach ($filters as $field => $filter) {
        if ($filter['exposed'] === TRUE) {
          $exposed_filters[$filter['expose']['identifier']] = $filter['expose']['identifier'];
          $form['field_mapping'][$field] = [
            '#type' => 'details',
            '#title' => $filter['id'],
            '#open' => TRUE,
            '#tree' => TRUE,
          ];
          $form['field_mapping'][$field]['field_name'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Field machine name'),
            '#default_value' => isset($field_mapping[$field]['field_name']) ? $field_mapping[$field]['field_name'] : '',
          ];
          $form['field_mapping'][$field]['field_property'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Field property'),
            '#default_value' => isset($field_mapping[$field]['field_property']) ? $field_mapping[$field]['field_property'] : '',
          ];
        }
      }
      $sort_exposed = FALSE;
      foreach ($sorts as $field => $sort) {
        if ($sort['exposed'] === TRUE) {
          $sort_exposed = TRUE;
          break;
        }
      }
      if ($sort_exposed === TRUE && empty($exposed_form_plugin->options['bef']['sort']['advanced']['combine'])) {
        $field = 'sort_by';
        $form['field_mapping'][$field] = [
          '#type' => 'details',
          '#title' => $this->t('Sort by'),
          '#open' => TRUE,
          '#tree' => TRUE,
        ];
        $form['field_mapping'][$field]['field_name'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Field machine name'),
          '#default_value' => isset($field_mapping[$field]['field_name']) ? $field_mapping[$field]['field_name'] : '',
        ];
        $form['field_mapping'][$field]['field_property'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Field property'),
          '#default_value' => isset($field_mapping[$field]['field_property']) ? $field_mapping[$field]['field_property'] : '',
        ];
        if (!empty($exposed_form_plugin->options['expose_sort_order'])) {
          $field = 'sort_order';
          $form['field_mapping'][$field] = [
            '#type' => 'details',
            '#title' => $this->t('Sort order'),
            '#open' => TRUE,
            '#tree' => TRUE,
          ];
          $form['field_mapping'][$field]['field_name'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Field machine name'),
            '#default_value' => isset($field_mapping[$field]['field_name']) ? $field_mapping[$field]['field_name'] : '',
          ];
          $form['field_mapping'][$field]['field_property'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Field property'),
            '#default_value' => isset($field_mapping[$field]['field_property']) ? $field_mapping[$field]['field_property'] : '',
          ];
        }
      }

      if (!empty($exposed_form_plugin->options['bef']['sort']['advanced']['combine'])) {
        $field = 'sort_bef_combine';
        $form['field_mapping'][$field] = [
          '#type' => 'details',
          '#title' => 'Combine sort order with sort by',
          '#open' => TRUE,
          '#tree' => TRUE,
        ];
        $form['field_mapping'][$field]['field_name'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Field machine name'),
          '#default_value' => isset($field_mapping[$field]['field_name']) ? $field_mapping[$field]['field_name'] : '',
        ];
        $form['field_mapping'][$field]['field_property'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Field property'),
          '#default_value' => isset($field_mapping[$field]['field_property']) ? $field_mapping[$field]['field_property'] : '',
        ];
      }
    }


    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity_views_filter_type = $this->entity;
    $status = $entity_views_filter_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()
          ->addMessage($this->t('Created the %label Entity views filter type.', [
            '%label' => $entity_views_filter_type->label(),
          ]));
        break;

      default:
        $this->messenger()
          ->addMessage($this->t('Saved the %label Entity views filter type.', [
            '%label' => $entity_views_filter_type->label(),
          ]));
    }
    $form_state->setRedirectUrl($entity_views_filter_type->toUrl('collection'));
  }

  public function changeDisplay(array &$form, FormStateInterface $form_state) {
    if ($selectedValue = $form_state->getValue('view_name')) {
      $selectedText = $form['view_name']['#options'][$selectedValue];
      $form['view_display_id']['#value'] = $selectedText;

    }
    return $form['view_display_id'];
  }

}
