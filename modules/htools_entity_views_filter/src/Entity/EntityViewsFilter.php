<?php

namespace Drupal\htools_entity_views_filter\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\views\Views;

/**
 * Defines the Entity views filter entity.
 *
 * @ingroup entity_views_filter
 *
 * @ContentEntityType(
 *   id = "entity_views_filter",
 *   label = @Translation("Entity views filter"),
 *   bundle_label = @Translation("Entity views filter type"),
 *   handlers = {
 *     "access" =
 *   "Drupal\htools_entity_views_filter\EntityViewsFilterAccessControlHandler",
 *     "storage" = "Drupal\Core\Entity\ContentEntityNullStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" =
 *   "Drupal\htools_entity_views_filter\EntityViewsFilterListBuilder",
 *
 *     "form" = {
 *       "default" =
 *   "Drupal\htools_entity_views_filter\Form\EntityViewsFilterForm",
 *       "add" =
 *   "Drupal\htools_entity_views_filter\Form\EntityViewsFilterForm",
 *       "edit" =
 *   "Drupal\htools_entity_views_filter\Form\EntityViewsFilterForm",
 *       "delete" =
 *   "Drupal\htools_entity_views_filter\Form\EntityViewsFilterDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" =
 *   "Drupal\htools_entity_views_filter\EntityViewsFilterHtmlRouteProvider",
 *     },
 *     "access" =
 *   "Drupal\htools_entity_views_filter\EntityViewsFilterAccessControlHandler",
 *   },
 *   admin_permission = "administer entity views filter entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *     "view_name" = "view_name",
 *     "view_display_id" = "view_display_id",
 *   },
 *   links = {
 *     "canonical" =
 *   "/admin/structure/entity_views_filter/{entity_views_filter}",
 *     "add-page" = "/admin/structure/entity_views_filter/add",
 *     "add-form" =
 *   "/admin/structure/entity_views_filter/add/{entity_views_filter_type}",
 *     "edit-form" =
 *   "/admin/structure/entity_views_filter/{entity_views_filter}/edit",
 *     "delete-form" =
 *   "/admin/structure/entity_views_filter/{entity_views_filter}/delete",
 *     "collection" = "/admin/structure/entity_views_filter",
 *   },
 *   bundle_entity_type = "entity_views_filter_type",
 *   field_ui_base_route = "entity.entity_views_filter_type.edit_form"
 * )
 */
class EntityViewsFilter extends ContentEntityBase implements EntityViewsFilterInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    return $fields;
  }

  /**
   *
   */
  public static function create(array $values = []) {
    $entity = parent::create($values);
    if (empty($values['type'])) {
      return $entity;
    }

    /** @var \Drupal\htools_entity_views_filter\Entity\EntityViewsFilterTypeInterface $bundle */
    $bundle = EntityViewsFilterType::load($values['type']);
    $field_mapping = $bundle->get('field_mapping');
    $view_name = $bundle->get('view_name');
    $view_display_id = $bundle->get('view_display_id');
    $view = Views::getView($view_name);
    $view->build($view_display_id);
    $filters = $view->getHandlers('filter');

    $query_string = \Drupal::requestStack()->getMasterRequest()->query->all();
    if (isset($query_string)) {
      foreach ($query_string as $field => $value) {
        foreach ($filters as $filter_id => $filter) {
          if ($filter['exposed'] === TRUE && $filter['expose']['identifier'] === $field) {
            $field_name = $field_mapping[$filter_id]['field_name'];
            if ($entity->hasField($field_name)) {
              $entity->set($field_name, $value);
            }
          }
        }
      }
      if (isset($query_string['sort_by'])) {
        $field_name = $field_mapping['sort_by']['field_name'];
        if ($entity->hasField($field_name)) {
          $entity->set($field_name, $query_string['sort_by']);
        }
      }
      if (isset($query_string['sort_order'])) {
        $field_name = $field_mapping['sort_order']['field_name'];
        if ($entity->hasField($field_name)) {
          $entity->set($field_name, $query_string['sort_order']);
        }
      }
      if (isset($query_string['sort_bef_combine'])) {
        $field_name = $field_mapping['sort_bef_combine']['field_name'];
        if ($entity->hasField($field_name)) {
          $entity->set($field_name, $query_string['sort_bef_combine']);
        }
      }
    }

    return $entity;
  }

}
