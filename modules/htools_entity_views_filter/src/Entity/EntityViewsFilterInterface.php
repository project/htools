<?php

namespace Drupal\htools_entity_views_filter\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Entity views filter entities.
 *
 * @ingroup entity_views_filter
 */
interface EntityViewsFilterInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Entity views filter name.
   *
   * @return string
   *   Name of the Entity views filter.
   */
  public function getName();

  /**
   * Sets the Entity views filter name.
   *
   * @param string $name
   *   The Entity views filter name.
   *
   * @return \Drupal\htools_entity_views_filter\Entity\EntityViewsFilterInterface
   *   The called Entity views filter entity.
   */
  public function setName($name);

  /**
   * Gets the Entity views filter creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Entity views filter.
   */
  public function getCreatedTime();

  /**
   * Sets the Entity views filter creation timestamp.
   *
   * @param int $timestamp
   *   The Entity views filter creation timestamp.
   *
   * @return \Drupal\htools_entity_views_filter\Entity\EntityViewsFilterInterface
   *   The called Entity views filter entity.
   */
  public function setCreatedTime($timestamp);

}
