<?php

namespace Drupal\htools_entity_views_filter\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Entity views filter type entities.
 */
interface EntityViewsFilterTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
