<?php

namespace Drupal\htools_entity_views_filter\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Entity views filter type entity.
 *
 * @ConfigEntityType(
 *   id = "entity_views_filter_type",
 *   label = @Translation("Entity views filter type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\htools_entity_views_filter\EntityViewsFilterTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\htools_entity_views_filter\Form\EntityViewsFilterTypeForm",
 *       "edit" = "Drupal\htools_entity_views_filter\Form\EntityViewsFilterTypeForm",
 *       "delete" = "Drupal\htools_entity_views_filter\Form\EntityViewsFilterTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\htools_entity_views_filter\EntityViewsFilterTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "entity_views_filter_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "entity_views_filter",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "view_name" = "view_name",
 *     "view_display_id" = "view_display_id",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "submit_button",
 *     "autosubmit",
 *     "autosubmit_hide",
 *     "view_name",
 *     "view_display_id",
 *     "field_mapping"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/entity_views_filter_type/{entity_views_filter_type}",
 *     "add-form" = "/admin/structure/entity_views_filter_type/add",
 *     "edit-form" = "/admin/structure/entity_views_filter_type/{entity_views_filter_type}/edit",
 *     "delete-form" = "/admin/structure/entity_views_filter_type/{entity_views_filter_type}/delete",
 *     "collection" = "/admin/structure/entity_views_filter_type"
 *   }
 * )
 */
class EntityViewsFilterType extends ConfigEntityBundleBase implements EntityViewsFilterTypeInterface {

  /**
   * The Entity views filter type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Entity views filter type label.
   *
   * @var string
   */
  protected $label;

}
