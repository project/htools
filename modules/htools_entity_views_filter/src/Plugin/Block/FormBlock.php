<?php

namespace Drupal\htools_entity_views_filter\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\htools_entity_views_filter\Entity\EntityViewsFilter;
use Drupal\views\Entity\View;
use Drupal\views\ViewExecutableFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a generic Views block.
 *
 * @Block(
 *   id = "entity_views_filter_form",
 *   admin_label = @Translation("Exposed form filter"),
 *   category = @Translation("Entity Views Filter"),
 * )
 */
class FormBlock extends BlockBase implements ContainerFactoryPluginInterface {

  protected $entityTypeManager;

  protected $entityFormBuilder;

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The module handler to invoke the alter hook.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  protected $prismaFormCache;

  protected $ajaxCapabilities;

  protected $formAction;

  /**
   * The factory to load a view executable with.
   *
   * @var \Drupal\views\ViewExecutableFactory
   */
  protected $executableFactory;

  /**
   *
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFormBuilderInterface $entity_form_builder, ClassResolverInterface $class_resolver, FormBuilderInterface $form_builder, TranslationInterface $string_translation, ModuleHandlerInterface $module_handler, ViewExecutableFactory $executable_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->entityFormBuilder = $entity_form_builder;
    $this->classResolver = $class_resolver;
    $this->formBuilder = $form_builder;
    $this->stringTranslation = $string_translation;
    $this->moduleHandler = $module_handler;
    $this->ajaxCapabilities = NULL;
    $this->formAction = NULL;
    $this->executableFactory = $executable_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity.form_builder'),
      $container->get('class_resolver'),
      $container->get('form_builder'),
      $container->get('string_translation'),
      $container->get('module_handler'),
      $container->get('views.executable')
    );
  }

  /**
   *
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $elements = parent::blockForm($form, $form_state);

    $configuration = $this->getConfiguration();
    $bundles = \Drupal::service('entity_type.bundle.info')
      ->getBundleInfo('entity_views_filter');
    foreach ($bundles as $bundle => $value) {
      $options[$bundle] = $value['label'];
    }

    $elements['bundle'] = [
      '#type' => 'select',
      '#title' => t('Search Form'),
      '#description' => t('Search Form'),
      '#default_value' => $configuration['bundle'],
      '#options' => $options,
      '#required' => TRUE,
      '#empty_option' => $this->t('Select one bundle'),
      '#empty_value' => '',
    ];

    $elements['form_display'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form display'),
      '#default_value' => $configuration['form_display'],
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['bundle'] = $form_state->getValue('bundle');
    $this->configuration['form_display'] = $form_state->getValue('form_display');
  }

  /**
   *
   */
  public function defaultConfiguration() {
    $default = parent::defaultConfiguration();

    $default['bundle'] = '';
    $default['form_display'] = 'default';

    return $default;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $configuration = $this->getConfiguration();
    $form_display = $configuration['form_display'];
    $bundle = $configuration['bundle'];
    $entity_type_id = 'entity_views_filter';
    $class = $this->entityTypeManager->getDefinition($entity_type_id, TRUE)
      ->getFormClass('default');
    $entity_views_filter = EntityViewsFilter::create(['type' => $bundle]);

    $view_name = $entity_views_filter->get('bundle')->entity->get('view_name');
    $display_id = $entity_views_filter->get('bundle')->entity->get('view_display_id');
    $entity_view = View::load($view_name);
    $view = $this->executableFactory->get($entity_view);
    $view->setDisplay($display_id);
    $view->display_handler->setOption('use_ajax', FALSE);
    $form_state_additions = [
      'view' => $view,
    ];

    $form_object_instance = $this->classResolver->getInstanceFromDefinition($class);

    $form_object = $form_object_instance
      ->setStringTranslation($this->stringTranslation)
      ->setModuleHandler($this->moduleHandler)
      ->setEntityTypeManager($this->entityTypeManager)
      ->setOperation($form_display)
      // The entity manager cannot be injected due to a circular dependency.
      // @todo Remove this set call in https://www.drupal.org/node/2603542.
      ->setEntityManager(\Drupal::entityManager());

    $form_object->setEntity($entity_views_filter);
    $form_state = (new FormState())->setFormState($form_state_additions);
    $form = $this->formBuilder->buildForm($form_object, $form_state);

    $form['#cache'] = [
      'max-age' => 0,
    ];

    return $form;
  }

}
