<?php

namespace Drupal\htools_entity_views_filter\Plugin\views\argument_default;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;

/**
 * Default argument plugin to select the typology.
 *
 * @ingroup views_argument_default_plugins
 *
 * @ViewsArgumentDefault(
 *   id = "typology",
 *   title = @Translation("Exposed filters for typology")
 * )
 */
class ExposedFiltersTipology extends ArgumentDefaultPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['codes'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['codes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Typology ID'),
      '#default_value' => $this->options['codes'],
      '#description' => $this->t('The ID of the products typology'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getArgument() {
    if ($this->options['codes']) {
      $uuids = [];
      $codes = explode(",", $this->options['codes']);

      $entities = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadByProperties(['ref_seccion' => $codes, 'vid' => 'categories']);

      foreach ($entities as $entity) {
        array_push($uuids, $entity->uuid());
      }
      $uuids = implode("+", $uuids);
      return $uuids;
    }

  }

}
