<?php

namespace Drupal\htools_entity_views_filter\Plugin\views\area;

use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\htools_entity_views_filter\Entity\EntityViewsFilter;
use Drupal\htools_entity_views_filter\Entity\EntityViewsFilterInterface;
use Drupal\htools_entity_views_filter\Form\EntityViewsFilterForm;
use Drupal\views\Plugin\views\area\TokenizeAreaPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Views area handler to display some configurable result summary.
 *
 * @ViewsArea("entity_views_filter")
 */
class BundleSummary extends TokenizeAreaPluginBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  /**
   * The entity form builder.
   *
   * @var \Drupal\Core\Entity\EntityFormBuilderInterface
   */
  protected $entityFormBuilder;

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The module handler to invoke the alter hook.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFormBuilderInterface $entity_form_builder, ClassResolverInterface $class_resolver, FormBuilderInterface $form_builder, TranslationInterface $string_translation, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFormBuilder = $entity_form_builder;
    $this->classResolver = $class_resolver;
    $this->formBuilder = $form_builder;
    $this->moduleHandler = $module_handler;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity.form_builder'),
      $container->get('class_resolver'),
      $container->get('form_builder'),
      $container->get('string_translation'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function preQuery() {
    if (isset($this->options['content']['value'])) {
      $content = $this->options['content']['value'];
      // Check for tokens that require a total row count.
      if (strpos($content, '[view:page-count]') !== FALSE || strpos($content, '[view:total-rows]') !== FALSE) {
        $this->view->get_total_rows = TRUE;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $options = [];
    $bundles = \Drupal::service('entity_type.bundle.info')
      ->getBundleInfo('entity_views_filter');
    foreach ($bundles as $bundle => $value) {
      $options[$bundle] = $value['label'];
    }

    $form['bundle'] = [
      '#type' => 'select',
      '#title' => t('Search Form'),
      '#description' => t('Search Form'),
      '#default_value' => $this->options['bundle'],
      '#options' => $options,
    ];

    $form['form_mode'] = [
      '#type' => 'textfield',
      '#title' => t('Form mode'),
      '#description' => t('Entity Views Filter form mode to render the search form'),
      '#default_value' => $this->options['form_mode'],
    ];

    $form['form_class'] = [
      '#type' => 'textfield',
      '#title' => t('Form class namespace'),
      '#description' => t('The form class namespace to use to handle the form. If empty, then will be use the default form class: ') . EntityViewsFilterForm::class,
      '#default_value' => $this->options['form_class'],
    ];

    $form['hide_form'] = [
      '#type' => 'checkbox',
      '#title' => t('Hide form'),
      '#default_value' => $this->options['hide_form'],
    ];

    $form['summary'] = [
      '#type' => 'textfield',
      '#title' => t('View mode'),
      '#description' => t('Entity Views Filter view mode to render the search summary'),
      '#default_value' => $this->options['summary'],
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    if ($empty && !$this->options['empty']) {
      return [];
    }

    $entity_views_filter = EntityViewsFilter::create(['type' => $this->options['bundle']]);

    $result = [
      'wrapper' => [
        '#type' => 'container',
        '#cache' => ['max-age' => 0],
      ],
    ];

    if (empty($this->options['summary'])) {
      $result['wrapper']['form'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['entity-views-filter-bundle-summary-form'],
        ],
        'block' => $this->buildForm($entity_views_filter),
      ];
    }

    if (!empty($this->options['summary'])) {
      $result['wrapper']['summary'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['entity-views-filter-bundle-summary-summary'],
        ],
        'block' => $this->buildSummary($entity_views_filter),
      ];
    }

    return $result;
  }

  /**
   *
   */
  public function buildSummary(EntityViewsFilterInterface $entity_views_filter) {
    $view_builder = $this->entityTypeManager->getViewBuilder('entity_views_filter');
    $language = \Drupal::languageManager()->getCurrentLanguage();
    return $view_builder->view($entity_views_filter, $this->options['summary'], $language->getId());
  }

  /**
   *
   */
  public function buildForm(EntityViewsFilterInterface $entity_views_filter) {
    $form_display = empty($this->options['form_mode']) ? 'default' : $this->options['form_mode'];
    $class = empty($this->options['form_class']) ? EntityViewsFilterForm::class : $this->options['form_class'];
    $filters = $entity_views_filter->getFields();
    foreach ($filters as $filter) {
      $filter_name = $filter->getName();
      $filter_type = $filter->getFieldDefinition()->getType();
      if ($filter_type == ('boolean') && (isset($filter->getValue()[0]))) {
        if ($filter->getValue()[0]['value'] === ('All')) {
          $entity_views_filter->set($filter_name, 0);
        }
      }
      elseif ($filter_type == ('list_string')) {
        $values = [];

        if (isset($filter->getValue()[0])) {
          $rawvalues = $filter->getValue()[0];
        }

        if (!empty($rawvalues)) {
          foreach ($rawvalues as $rawvalue) {
            $values[] = $rawvalue;
          }
          $entity_views_filter->set($filter_name, $values);
        }
      }
    }

    $form_state_additions = [
      'view' => $this->view,
    ];

    $form_object = $this->classResolver->getInstanceFromDefinition($class);

    $form_object->setStringTranslation($this->stringTranslation)
      ->setModuleHandler($this->moduleHandler)
      ->setEntityTypeManager($this->entityTypeManager)
      ->setOperation($form_display);

    $form_object->setEntity($entity_views_filter);
    $form_state = (new FormState())->setFormState($form_state_additions);

    $ajax_refreshing = !empty($this->view->ajaxRefreshing);
    $form_object->setAjaxRefreshing($ajax_refreshing);
    $form = $this->formBuilder->buildForm($form_object, $form_state);

    $form['#cache'] = [
      'max-age' => 0,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['bundle'] = [
      'default' => '',
    ];
    $options['form_mode'] = [
      'default' => 'default',
    ];
    $options['form_class'] = [
      'default' => '',
    ];
    $options['hide_form'] = [
      'default' => FALSE,
    ];
    $options['summary'] = [
      'default' => '',
    ];

    return $options;
  }

}
