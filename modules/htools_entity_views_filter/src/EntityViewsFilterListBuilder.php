<?php

namespace Drupal\htools_entity_views_filter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Entity views filter entities.
 *
 * @ingroup entity_views_filter
 */
class EntityViewsFilterListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Entity views filter ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\htools_entity_views_filter\Entity\EntityViewsFilter $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.entity_views_filter.edit_form',
      ['entity_views_filter' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
