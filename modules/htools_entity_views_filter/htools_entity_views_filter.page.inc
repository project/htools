<?php

/**
 * @file
 * Contains entity_views_filter.page.inc.
 *
 * Page callback for Entity views filter entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Entity views filter templates.
 *
 * Default template: entity_views_filter.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_entity_views_filter(array &$variables) {
  // Fetch EntityViewsFilter Entity Object.
  $entity_views_filter = $variables['elements']['#entity_views_filter'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
