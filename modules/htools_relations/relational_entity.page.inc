<?php

/**
 * @file
 * Contains relational_entity.page.inc.
 *
 * Page callback for Relational entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Relational entity templates.
 *
 * Default template: relational_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_relational_entity(array &$variables) {
  // Fetch RelationalEntity Entity Object.
  $relational_entity = $variables['elements']['#relational_entity'];

  unset($variables['elements']['uid']);
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
