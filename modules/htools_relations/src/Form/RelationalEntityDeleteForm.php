<?php

namespace Drupal\htools_relations\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Relational entity entities.
 *
 * @ingroup htools_relations
 */
class RelationalEntityDeleteForm extends ContentEntityDeleteForm {


}
