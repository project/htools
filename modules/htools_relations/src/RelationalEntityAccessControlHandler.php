<?php

namespace Drupal\htools_relations;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Relational entity entity.
 *
 * @see \Drupal\htools_relations\Entity\RelationalEntity.
 */
class RelationalEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\htools_relations\Entity\RelationalEntityInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished relational entity entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published relational entity entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit relational entity entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete relational entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add relational entity entities');
  }


}
