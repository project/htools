<?php

namespace Drupal\htools_relations\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Relational entity type entity.
 *
 * @ConfigEntityType(
 *   id = "relational_entity_type",
 *   label = @Translation("Relational entity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\htools_relations\RelationalEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\htools_relations\Form\RelationalEntityTypeForm",
 *       "edit" = "Drupal\htools_relations\Form\RelationalEntityTypeForm",
 *       "delete" = "Drupal\htools_relations\Form\RelationalEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\htools_relations\RelationalEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "relational_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "relational_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/relational_entity_type/{relational_entity_type}",
 *     "add-form" = "/admin/structure/relational_entity_type/add",
 *     "edit-form" = "/admin/structure/relational_entity_type/{relational_entity_type}/edit",
 *     "delete-form" = "/admin/structure/relational_entity_type/{relational_entity_type}/delete",
 *     "collection" = "/admin/structure/relational_entity_type"
 *   }
 * )
 */
class RelationalEntityType extends ConfigEntityBundleBase implements RelationalEntityTypeInterface {

  /**
   * The Relational entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Relational entity type label.
   *
   * @var string
   */
  protected $label;

}
