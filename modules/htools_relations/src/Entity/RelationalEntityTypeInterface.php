<?php

namespace Drupal\htools_relations\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Relational entity type entities.
 */
interface RelationalEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
