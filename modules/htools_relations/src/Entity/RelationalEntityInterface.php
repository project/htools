<?php

namespace Drupal\htools_relations\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Relational entity entities.
 *
 * @ingroup htools_relations
 */
interface RelationalEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Relational entity name.
   *
   * @return string
   *   Name of the Relational entity.
   */
  public function getName();

  /**
   * Sets the Relational entity name.
   *
   * @param string $name
   *   The Relational entity name.
   *
   * @return \Drupal\htools_relations\Entity\RelationalEntityInterface
   *   The called Relational entity entity.
   */
  public function setName($name);

  /**
   * Gets the Relational entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Relational entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Relational entity creation timestamp.
   *
   * @param int $timestamp
   *   The Relational entity creation timestamp.
   *
   * @return \Drupal\htools_relations\Entity\RelationalEntityInterface
   *   The called Relational entity entity.
   */
  public function setCreatedTime($timestamp);

}
