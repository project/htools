<?php

namespace Drupal\htools_migrate\Plugin\Action;


use Drupal\Core\Action\ActionBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\field\Entity\FieldConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Rebuild remote medias of a content.
 *
 * @Action(
 *   id = "rebuild_remote_media",
 *   label = @Translation("Rebuild remote media"),
 *   action_label = @Translation("Rebuild remote media"),
 *   confirm_form_route_name = "htools_migrate.rebuild_remote_media_confirm",
 *   deriver =
 *   "Drupal\htools_migrate\Plugin\Action\Derivative\RebuildRemoteMediaByContentDeriver"
 * )
 */
class RebuildRemoteMediaByContent extends ActionBase implements ContainerFactoryPluginInterface {

  /**
   * The tempstore factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a CancelUser object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PrivateTempStoreFactory $temp_store_factory, AccountInterface $current_user) {
    $this->currentUser = $current_user;
    $this->tempStoreFactory = $temp_store_factory;

    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('tempstore.private'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return TRUE;
  }

  /**
   * Get the field options.
   *
   * @param array $fields
   *   The array of fields.
   *
   * @return array
   *   The select options.
   */
  public static function getMediaFields(array $fields) {
    $options = [];
    $type = 'entity_reference';
    foreach ($fields as $field) {
      if ($field instanceof FieldConfig) {
        if ($field->getType() === $type) {
          $settings = $field->getSettings();
          if ($settings['handler'] === 'default:media') {
            $options[$field->get('field_name')] = $field;
          }
        }
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $entities) {
    $this->tempStoreFactory->get('htools_migrate_rebuild_remote_media')
      ->set($this->currentUser->id(), $entities);
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    $this->executeMultiple([$entity]);
  }

  /**
   * {@inheritdoc}
   */
  public static function rebuild($entity = NULL, $keep_original_name = FALSE, array $fields = NULL) {
    $queue = \Drupal::service('queue')->get('download_remote_file');
    if ($fields === NULL || empty($fields)) {
      $fields = static::getMediaFields($entity->getFieldDefinitions());
    }
    foreach ($fields as $field_name => $field) {
      if (!$entity->get($field_name)->isEmpty()) {
        foreach ($entity->get($field_name) as $item) {
          /* @var \Drupal\media\MediaInterface $media */
          $media = $item->entity;
          if ($media->hasField('remote_source') && !$media->get('remote_source')
              ->isEmpty()) {
            $remote_source = $media->get('remote_source')->value;
          }
          else {
            return;
          }
          $source = $media->bundle->entity->getSource()->getConfiguration();
          $queue->createItem([
            'type' => $media->getEntityTypeId(),
            'bundle' => $media->bundle(),
            'id' => $media->id(),
            'source' => $remote_source,
            'field' => $source['source_field'],
            'keep_original_file_name' => $keep_original_name,
            'properties' => [],
            // @FIXME Use DI.
            'attempts' => \Drupal::state()
              ->get('remote_file_download_attempts', 288),
          ]);
        }
      }
    }
  }

}
