<?php

namespace Drupal\htools_migrate\Plugin\migrate\id_map;

use Drupal\Component\Utility\Unicode;
use Drupal\migrate\Plugin\migrate\id_map\Sql;

/**
 * Extends SQL id_map plugin.
 *
 * This plugin allow to share id_mapping between migrations. The main use case
 * is when you have a destination entity which may come from multiple sources,
 * but you want to avoid duplicates.
 *
 * @PluginID("sql_shared_map")
 */
class SqlSharedMap extends Sql {

  /**
   * {@inheritdoc}
   */
  protected function init() {
    if (!$this->initialized) {
      $this->initialized = TRUE;
      // Default generated table names, limited to 63 characters.
      $machine_name = str_replace(':', '__', $this->migration->id());
      $migration = $this->migration->getPluginDefinition();
      if (!empty($migration['third_party_settings']['sql_shared_map']['migration_id'])) {
        $machine_name = str_replace(':', '__', $migration['third_party_settings']['sql_shared_map']['migration_id']);
      }
      $prefix_length = strlen($this->getDatabase()->tablePrefix());
      $this->mapTableName = 'migrate_map_' . strtolower($machine_name);
      $this->mapTableName = substr($this->mapTableName, 0, 63 - $prefix_length);
      $this->messageTableName = 'migrate_message_' . strtolower($machine_name);
      $this->messageTableName = substr($this->messageTableName, 0, 63 - $prefix_length);
      $this->ensureTables();
    }
  }

  /**
   * Get migration plugin definition.
   *
   * @return array
   *   Migration plugin definition.
   */
  public function getMigration(): ?array {
    return $this->migration->getPluginDefinition();
  }

  /**
   * The source ID fields.
   *
   * @return array
   *   The source ID fields.
   */
  public function sourceMapIdFields() {
    return $this->sourceIdFields();
  }

}
