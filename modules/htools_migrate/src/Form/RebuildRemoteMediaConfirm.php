<?php

namespace Drupal\htools_migrate\Form;

use Drupal\Core\DependencyInjection\DeprecatedServicePropertyTrait;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Drupal\field\FieldStorageConfigInterface;
use Drupal\htools_migrate\Plugin\Action\RebuildRemoteMediaByContent;
use Drupal\user\UserStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a confirmation form for cancelling multiple user accounts.
 *
 * @internal
 */
class RebuildRemoteMediaConfirm extends ConfirmFormBase {

  use DeprecatedServicePropertyTrait;

  /**
   * {@inheritdoc}
   */
  protected $deprecatedProperties = ['entityManager' => 'entity.manager'];

  /**
   * The temp store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypePluginManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a new UserMultipleCancelConfirm.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The temp store factory.
   * @param \Drupal\user\UserStorageInterface $user_storage
   *   The user storage.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory, UserStorageInterface $user_storage, EntityTypeManagerInterface $entity_type_manager, FieldTypePluginManagerInterface $field_type_plugin_manager, EntityFieldManagerInterface $entity_field_manager = NULL) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->userStorage = $user_storage;
    $this->fieldTypePluginManager = $field_type_plugin_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rebuild_remote_media_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Select fields with remote medias to rebuild');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.user.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Rebuild remote medias');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $entities = $this->tempStoreFactory
      ->get('htools_migrate_rebuild_remote_media')
      ->get($this->currentUser()->id());
    if (!empty($entities)) {
      $entity = reset($entities);
      if (!$entity instanceof ContentEntityBase) {
        throw new \Exception('No entities selected');
      }
    }
    else {
      throw new \Exception('No entities selected');
    }

    $form = parent::buildForm($form, $form_state);

    $options = $this->getExistingFieldStorageOptions($entity->getEntityTypeId());
    $form['media_fields'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select media fields to rebuild'),
      '#options' => $options,
    ];

    $form['keep_original_file_name'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Keep original file name'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entities = $this->tempStoreFactory
      ->get('htools_migrate_rebuild_remote_media')
      ->get($this->currentUser()->id());

    $fields = array_filter($form_state->getValue('media_fields'));
    $keep = (bool) $form_state->getValue('keep_original_file_name');
    foreach ($entities as $entity) {
      RebuildRemoteMediaByContent::rebuild($entity, $keep, $fields);
    }

    $messenger = \Drupal::messenger();
    $message = $this->t('%count items send to queue to rebuild their remote medias.', [
      '%count',
      count($entities),
    ]);
    $messenger->addMessage($message, 'status', FALSE);

    $current_user_id = $this->currentUser()->id();
    // Clear out the accounts from the temp store.
    $this->tempStoreFactory->get('htools_migrate_rebuild_remote_media')
      ->delete($current_user_id);
  }

  /**
   * Returns an array of existing field storages that can be added to a bundle.
   *
   * @return array
   *   An array of existing field storages keyed by name.
   */
  protected function getExistingFieldStorageOptions($entity_type_id) {
    $options = [];
    // Load the field_storages and build the list of options.
    $field_types = $this->fieldTypePluginManager->getDefinitions();
    foreach ($this->entityFieldManager->getFieldStorageDefinitions($entity_type_id) as $field_name => $field_storage) {
      $settings = $field_storage->getSettings();
      $field_type = $field_storage->getType();
      if ($field_storage instanceof FieldStorageConfigInterface
        && !$field_storage->isLocked()
        && empty($field_types[$field_type]['no_ui'])
        && !in_array($this->bundle, $field_storage->getBundles(), TRUE)
        && $field_type === 'entity_reference'
        && !empty($settings['target_type'])
        && $settings['target_type'] === 'media') {
        $options[$field_name] = $this->t('@type: @field', [
          '@type' => $field_types[$field_type]['label'],
          '@field' => $field_name,
        ]);
      }
    }
    asort($options);

    return $options;
  }

}
